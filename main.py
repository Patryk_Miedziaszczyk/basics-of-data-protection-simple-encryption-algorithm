from tkinter import *

def isSpace(char):
   if char == ' ':
       return True
   else:
       return False

def removeSpecialCharacters(word):
   iterator = 0
   newWord = []
   word = list(word)
   while iterator != len(word):
       if 32 <= ord(word[iterator]) <= 64 or 91 <= ord(word[iterator]) <= 94 or ord(word[iterator]) == 96 or 123 <= ord(word[iterator]) <= 126:
           iterator += 1
           continue
       else:
           newWord.append(word[iterator])
           iterator += 1
   return newWord

def keyGenerator(key, word):
   word = removeSpecialCharacters(word)
   if len(word) > len(key):
       j = 0
       iterator = 0
       newKey = []
       while j != len(word):
           if iterator == len(key):
               iterator = 0
           if 'a' <= key[iterator] <= 'z':
               key[iterator] = chr(ord(key[iterator]) - 32)
           newKey.append(key[iterator])
           iterator += 1
           j += 1
       return newKey
   elif len(word) <= len(key):
       newKey = []
       iterator = 0
       while iterator != len(word):
           if 'a' <= key[iterator] <= 'z':
               key[iterator] = chr(ord(key[iterator]) - 32)
           newKey.append(key[iterator])
           iterator += 1
       return newKey


def encryptorText(key, word):
   newKey = keyGenerator(key, word)
   encrypted = []
   keyIterator = 0
   wordIterator = 0
   while wordIterator != len(word):
       if 'a' <= word[wordIterator] <= 'z':
           upperChar = chr(ord(word[wordIterator]) - 32)
           encryptedChar = ((ord(upperChar) + ord(newKey[keyIterator])) % 26) + ord('A') + 32
           encrypted.append(chr(encryptedChar))
           keyIterator += 1
       elif 65 <= ord(word[wordIterator]) <= 90:
           encryptedChar = ((ord(word[wordIterator]) + ord(newKey[keyIterator])) % 26) + ord('A')
           encrypted.append(chr(encryptedChar))
           keyIterator += 1
       elif 32 <= ord(word[wordIterator]) <= 64 or 91 <= ord(word[wordIterator]) <= 96 or 123 <= ord(
               word[wordIterator]) <= 126:
           encrypted.append(word[wordIterator])
       else:
           print('Nieznany znak')
       wordIterator += 1
   finalString = ''.join(encrypted)
   return finalString


def decryptorText(key, word):
   newKey = keyGenerator(key, word)
   decrypted = []
   keyIterator = 0
   wordIterator = 0
   while wordIterator != len(word):
       if 'a' <= word[wordIterator] <= 'z':
           word[wordIterator] = chr(ord(word[wordIterator]) - 32)
           decryptedChar = ((ord(word[wordIterator]) - ord(newKey[keyIterator]) + 26) % 26) + ord('A') + 32
           decrypted.append(chr(decryptedChar))
           keyIterator += 1
       elif 32 <= ord(word[wordIterator]) <= 64 or 91 <= ord(word[wordIterator]) <= 96 or 123 <= ord(
               word[wordIterator]) <= 126:
           decrypted.append(word[wordIterator])
       else:
           decryptedChar = ((ord(word[wordIterator]) - ord(newKey[keyIterator]) + 26) % 26) + ord('A')
           decrypted.append(chr(decryptedChar))
           keyIterator += 1
       wordIterator += 1
   finalString = ''.join(decrypted)
   return finalString

def getEntry():
   key = list(var.get())
   if var1.get() == 1 and var2.get() == 0 and var3.get() == 0:
       information.delete(1.0, 'end')
       decode.delete(1.0,'end')
       tekst = list(encode.get("1.0", "end-1c"))
       data = encryptorText(key,tekst)
       decode.insert(1.0, data)
   elif var1.get() == 0 and var2.get() == 1 and var3.get() == 0:
       information.delete(1.0, 'end')
       decode.delete(1.0,'end')
       tekst = list(encode.get("1.0", "end-1c"))
       data = decryptorText(key, tekst)
       decode.insert(1.0, data)
   elif var1.get() == 1 and var2.get() == 0 and var3.get() == 1:
       information.delete(1.0, 'end')
       decode.delete(1.0,'end')
       tekst = list(encode.get("1.0", "end-1c"))
       data = encryptorText(key, tekst)
       f = open('D:\Dane\Pulpit\Semestr 5\Podstawy Ochrony Danych\Laboratoria 1\plik.txt', 'w')
       f.write(data)
       information.insert(1.0, 'Dane zapisane do pliku!')
   elif var1.get() == 0 and var2.get() == 1 and var3.get() == 1:
       information.delete(1.0, 'end')
       decode.delete(1.0, 'end')
       information.insert(1.0, 'Dane pobrane z pliku!')
       f = open('D:\Dane\Pulpit\Semestr 5\Podstawy Ochrony Danych\Laboratoria 1\plik.txt', 'r')
       tekst = list(f.read())
       data = decryptorText(key, tekst)
       decode.insert(1.0, data)
   else:
       information.delete(1.0, 'end')
       decode.delete(1.0, 'end')
       information.insert(1.0, '!Błąd prosze zaznaczyć poprawne checkboxy!')


application = Tk()
menu = Frame(master=application, width=200, bg="#BDCCFF", height=768)
menu.pack(side="left")
menuName = Label(menu, text="Vigenere Cipher", bg="#BDCCFF", font=("Courier", 14)).place(x=6, y=10)
main = Frame(master=application, width=824, bg="#7AB1FF", height=768)
main.pack(side="left")
var = StringVar()
name = Label(menu, text="Klucz", bg="#BDCCFF", font=("Courier", 12)).place(x=10, y=100)
e1 = Entry(menu,textvariable=var).place(x=70, y=100)
encodeName = Label(main, text="Tekst", bg="#BDCCFF", font=("Courier", 12))
encodeName.place(x=10, y=100)
encode = Text(main, width=80, height=10)
encode.place(x=150, y=100)
decodeName = Label(main, text="Tekst", bg="#BDCCFF", font=("Courier", 12))
decodeName.place(x=10, y=400)
decode = Text(main, width=80, height=10)
decode.place(x=150, y=400)
informationName = Label(main, text="Information", bg="#BDCCFF", font=("Courier", 12))
informationName.place(x=10, y=600)
information = Text(main, width=80, height=4, font=("Courier", 16), fg='red')
information.place(x=150, y=600)
sbmitbtn = Button(menu, text="Konwertuj", activebackground="pink", activeforeground="blue", command=getEntry).place(x=30, y=350)
var1 = IntVar()
var2 = IntVar()
var3 = IntVar()
c1 = Checkbutton(menu, text='Kodowanie', variable=var1, onvalue=1, offvalue=0, bg="#BDCCFF", font=("Courier", 12)).place(x=30, y=200)
c2 = Checkbutton(menu, text='Dekodowanie', variable=var2, onvalue=1, offvalue=0, bg="#BDCCFF", font=("Courier", 12)).place(x=30, y=250)
c3 = Checkbutton(menu, text='Plik', variable=var3, onvalue=1, offvalue=0, bg="#BDCCFF", font=("Courier", 12)).place(x=30, y=300)
application.mainloop()

